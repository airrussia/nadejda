<?php

sleep(1);

header('Content-Type: application/json');

/**
 * Таблица регионов
 * id - идентификатор региона
 * name - название региона
 */
$region = array(
    array('id' => 1, 'name' => 'Краснояский край'),
    array('id' => 2, 'name' => 'Республика хакасия')
);

/**
 * Таблица городов
 * id - идентикатор города
 * rid - принадлежность города к региону
 * name - название города
 */
$city = array(
    array('id'  =>  1, 'rid'    => 1, 'name'    =>  'Красноярск'),
    array('id'  =>  2, 'rid'    => 1, 'name'    =>  'Ачинск'),
    array('id'  =>  3, 'rid'    => 1, 'name'    =>  'Канск'),
    array('id'  =>  4, 'rid'    => 1, 'name'    =>  'прочие'),
    
    array('id'  =>  5, 'rid'    => 2, 'name'    =>  'Абакан'),
    array('id'  =>  6, 'rid'    => 2, 'name'    =>  'Саяногорск'),
    array('id'  =>  7, 'rid'    => 2, 'name'    =>  'прочие'),
);

/**
 * Таблица территориальных коэфициентов, 
 * где 
 *  сid - город из таблицы city, 
 *  right - лицо [1 - физ, 2 - юр.], 
 *  rate - коэфицент
 */
$factor = array( 
    array('cid' => 1, 'right' => 1, 'rate' => 1.8),
    array('cid' => 1, 'right' => 2, 'rate' => 1.0),
    array('cid' => 2, 'right' => 1, 'rate' => 1.1),
    array('cid' => 2, 'right' => 2, 'rate' => 0.8),
    array('cid' => 3, 'right' => 1, 'rate' => 1.0),
    array('cid' => 3, 'right' => 2, 'rate' => 0.8),
    array('cid' => 4, 'right' => 1, 'rate' => 0.9),
    array('cid' => 4, 'right' => 2, 'rate' => 0.5),
    array('cid' => 5, 'right' => 1, 'rate' => 1.0),
    array('cid' => 5, 'right' => 2, 'rate' => 0.9),
    array('cid' => 6, 'right' => 1, 'rate' => 1.0),
    array('cid' => 6, 'right' => 2, 'rate' => 0.9),
    array('cid' => 7, 'right' => 1, 'rate' => 1.0),
    array('cid' => 7, 'right' => 2, 'rate' => 0.9)
);

/**
 * Тип техники 
 * id - идентификатор техники
 * name - название техники
 */
$type = array(
    array('id' => 1, 'name' => 'Легковой автомобиль'),
    array('id' => 2, 'name' => 'Грузовой автомобиль'),
    array('id' => 3, 'name' => 'Автобус')
);

/**
 * Дополнительные параметры использование техники 
 * id - идентификатор дополнительного параметра
 * tid - к какой техники дополнительный параметры имеет отношение
 * name - название 
 */
$use_type = array(
    array('id' => 1, 'tid' => 1, 'name' => 'используемый в качестве такси'),
    array('id' => 2, 'tid' => 2, 'name' => 'с разрешенной максимальной массой свыше 16 тонн'),
    array('id' => 3, 'tid' => 3, 'name' => 'с числом пассажирский мест более 16'),    
);

/**
 * Таблица базовых тарифов
 * tid - id из таблицы техники
 * use - id из таблицы дополнительных параметров
 * price - Основной тариф в рублях
 */
$baserate = array(
    array('tid' => 1, 'use' => 0, 'price' => 4118),
    array('tid' => 1, 'use' => 1, 'price' => 6166),
    array('tid' => 2, 'use' => 0, 'price' => 3509),
    array('tid' => 2, 'use' => 2, 'price' => 5284),
    array('tid' => 3, 'use' => 0, 'price' => 2808),
    array('tid' => 3, 'use' => 3, 'price' => 3509),
);


$db = array(
    'region'    =>  $region,
    'city'      =>  $city,
    'factor'    =>  $factor,
    'type'      =>  $type,
    'use_type'  =>  $use_type,
    'baserate'  =>  $baserate
);


echo json_encode($db);
