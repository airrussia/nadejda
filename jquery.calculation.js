(function ($) {

    $.fn.calculation = function (options) {

        var options = $.extend({
            url: 'db.php', //параметр1
            resultTo: '#result', //параметр2
            database: ''
        }, options);

        var init = function () {

            $this = $(this);

            $.getJSON(options.url, function (data) {
                options.database = data;
                render($this);
            });
        }

        var render = function (to) {

            var form = $("<form></form>").appendTo(to);

            var right = $("<div></div>").addClass("radio").appendTo(form);

            var rights = [{value: 1, name: 'Физическое лицо', checked: true}, {value: 2, name: 'Юридическое лицо', checked: false}];

            for (i in rights) {
                right.append("<label><input type=\"radio\" name=\"right\" value=\"" + rights[i].value + "\" " + ((rights[i].checked == true) ? "checked" : "") + "/>" + rights[i].name + "</label>");
            }

            // Типы транспорта
            var select_tid = $("<select></select>").attr({class: "form-control", name: "tid"}).appendTo(form);            
            $.each(options.database.type, function () {
                $("<option></option>").val(this.id).text(this.name).appendTo(select_tid);
            });
            
            var use = $("<div></div>");
            select_tid.after(use);
            select_tid.change(function () {
                var $this = $(this);
                for (i in options.database.use_type) {
                    if ($this.val() == options.database.use_type[i].tid) {
                        use.html("<div class=\"checkbox\"><label><input type=\"checkbox\" name=\"use_type\" value=\"" + options.database.use_type[i].id + "\">" + options.database.use_type[i].name + "</label></div>");
                    }
                }
            }).change();


            // Строим значение для select region 
            var select_rid = $("<select></select>").attr({class: "form-control", name: "cid"}).appendTo(form);

            $.each(options.database.region, function () {
                var optgroup = $("<optgroup></optgroup>").attr({label: this.name, id: this.id}).appendTo(select_rid);
                var rid = this.id;
                $.each(options.database.city, function () {
                    if (rid == this.rid) {
                        var option = $("<option></option>").val(this.id).text(this.name).appendTo(optgroup);
                    }
                });
            });

            var result = $("<div></div>").attr({class: 'result'});            
            form.append(result);
            
            form.change(function () {
                var params = $(this).serializeArray();
                result.html("<div><p>Стоимость страховки</p><span class=\"sum\">"+Calc(params)+"</span><span class=\"rub\">&nbsp;pуб.</span></div>");
            }).change();
            
        }

        /**
         * Производит расчет стоимости страховки
         * @param {type} params
         * @returns {result|Number}
         */
        function Calc(params) {
            params = normalized(params);
            var f = GetFactory(params.cid, params.right);
            var r = GetRate(params.tid, params.use_type);
            var s = f * r;

            result = Math.ceil(s);
            return result;
        }

        /**
         * Нормализация данных от SerializeArray -> Object JSON
         * Нужна для более простого обращения к значения полей. 
         * @param {json array objects} params
         * @returns {json object}
         */
        function normalized(params) {
            var result = {
                tid: 0,
                cid: 0,
                use_type: 0,
                right: 0
            }

            for (i in params) {
                if (params[i].name in result) {
                    result[params[i].name] = parseInt(params[i].value);
                }
            }

            return result;
        }

        /**
         * Производит выборку из таблицы территориальных коэфициентов
         * @param {integer} cid
         * @param {integer} right
         * @returns {integer|Boolean}
         */
        function GetFactory(cid, right) {
            var result = false;
            $.each(options.database.factor, function () {
                if ((this.cid == cid) && (this.right == right)) {
                    result = this.rate;
                    return false;   // break функции each
                }
            });
            return result;
        }

        /**
         * Выборка из таблицы тарифов
         * @param {integer} tid
         * @param {integer} use
         * @returns {integer|Boolean}
         */
        function GetRate(tid, use) {
            var result = false;
            $.each(options.database.baserate, function () {
                if ((this.tid == tid) && (this.use == use)) {
                    result = this.price;
                    return false;   // break функции each
                }
            });
            return result;
        }


        return this.each(init);
    }

})(jQuery);