# Задание: Создать форму расчета стоимости страховки.

## Элементы на форме
На форме должны быть следующие элементы.

1. Организационно-правовая форма (RADIOBUTTON):
Физическое лицо
Юридическое лицо

2. Категория транспортного средства:
Выбор из SELECT
Дополнительный выбор по CHECKBOX
Легковой автомобиль
Возможна опция “Используется в качетсве такси”
Грузовой автомобиль
Возможна опция “с разрешенной максимальной массой свыше 16 тонн”
Автобус
Возможна опция “с числом пассажирских мест более 16”

3. Выбор региона (SELECT объединить регионы через тег <optgroup> )
* Красноярский край
* Красноярск
* Ачинск
* Канск
* прочие
* Республика Хакасия
* Абакан
* Саяногорск
* прочие

## Данные для расчета

Тарифы и коэффициенты используемые для расчета

Тип ТС                                                                  Основной тариф (руб)
Легковой автомобиль                                                     4118
Легковой автомобиль(используемый в качестве такси)                      6166
Грузовой автомобиль                                                     3509
Грузовой автомобиль с разрешенной максимальной массой свыше 16 тонн     5284
Автобус                                                                 2808
Автобус с числом пассажирский мест более 16                             3509

Коэффициенты для расчета
Населенный пункт             Поправочный коэффициент территории
                             Для Физ.лиц             Для Юр.лиц
Красноярский край
Красноярск                   1.8                     1
Ачинск                       1.1                     0.8
Канск                        1                       0.8
прочие                       0.9                     0.5
Республика Хакасия
Абакан                       1                       0.9
Саяногорск                   0.9                     0.8
прочие                       0.6                     0.5

## Формула расчета
Стоимость страховки = Основной тариф * поправочный коэффициент

Тарифы и коэффициенты должны храниться в ассоциативном массиве, в отдельном файле PHP обращение к которому производится средствами ajax, ответ получаем в формате json
Расчет должен производиться на стороне клиента средствами JS